% \documentclass[a4paper,10pt]{article}
% \usepackage[utf8]{inputenc}
% \usepackage{graphicx}
% \usepackage{amsmath}
% 
% %opening
% \title{The PSF profiles used by PampelMuse}
% \author{Sebastian Kamann}
% % \allowdisplaybreaks
% 
% %%%%%%%%%%%%%% Own definitions %%%%%%%%%%%%%%%%%%%%%
% \newcommand{\fwhm}{\text{FWHM}}
% \newcommand{\rd}{r_\text{d}}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% \begin{document}
% 
% \maketitle

\graphicspath{{./../images/}}

\section{The PSF profiles}
\label{app:profiles}

% \begin{abstract}
An overview of the functional forms of the analytical PSF profiles that PampelMuse uses. Besides the functional forms, we also provide the partial derivatives with respect to the individual profile parameters. They are used to determine the Jacobi matrix in the Levenberg-Marquard algorithm that performs the non-linear least squares fit.
%\end{abstract}

\subsection{Handling of positions}
\label{sec:positions}

All profiles are defined as a function of $r$, with $r$ giving the distance of a pixel ($x_i$, $y_i$) to the centroid. In the most general case, the PSF will be of elliptical shape, with its semi-major rotated by an angle $\theta$ \textbf{with respect to the $y$-axis}. For this reason, we define a coordinate system (${\hat x}$, ${\hat y}$), that has the same origin as the PSF profile and has the ${\hat x}$-axis aligned with the semi-major axis. Assuming the central coordinates of profile $n$ ($n\in[0,N)$) in the original system are ($x^n$, $y^n$), the transformation is defined as
%
\begin{align} 
 {\hat x} = (x-x^n)\sin\theta - (y-y^n)\cos\theta\,,
\label{eq:psf_x} \\
%
 {\hat y} = (x-x^n)\cos\theta + (y-y^n)\sin\theta\,.
\label{eq:psf_y}
\end{align}
%
The radius $r$ is measured in units of pixels in direction of the semi-major axis of the profile, that is, for an arbitrary position angle $\theta$ with respect to that axis, $r$ is calculated using
%
\begin{equation} \label{eq:psf_radius}
 r(x, y) = \sqrt{{\hat x}^2 + \left( \frac{\hat y}{1-e} \right)^2} \,,
\end{equation}
%
where $e$ is the ellipticity of the profile.

The above equations imply that $r$ is a function of $2N+2$ parameters that can be free fit parameters in the PSF optimization: $x^n$, $y^n$, $\theta$, and $e$. The partial derivatives of $r$ with respect to those parameters are
%
\begin{align}
 \partial_{x^n} r \equiv \frac{\partial r}{\partial x^n} =& \left[{\hat x}^2 + \left(\frac{\hat y}{1-e}\right)^2\right]^{-1/2} \frac{1}{2} \left[2{\hat x}\frac{\partial {\hat x}}{\partial x^n} + \frac{2{\hat y}}{(1-e)^2} \frac{\partial {\hat y}}{\partial x^n}\right] \nonumber\\
 =& -\frac{1}{r}\left[ {\hat x}\sin\theta  + \frac{{\hat y}\cos\theta}{(1-e)^2}\right]\,, \displaybreak[0]\\ 
%
 \partial_{y^n} r \equiv \frac{\partial r}{\partial y^n} =& \left[{\hat x}^2 + \left(\frac{\hat y}{1-e}\right)^2\right]^{-1/2} \frac{1}{2} \left[2{\hat x}\frac{\partial {\hat x}}{\partial y^n} + \frac{2{\hat y}}{(1-e)^2} \frac{\partial {\hat y}}{\partial y^n}\right] \nonumber\\
 =& \frac{1}{r}\left[ {\hat x}\cos\theta  - \frac{{\hat y}\sin\theta}{(1-e)^2}\right]\,, \\
 %
 \partial_{\theta} r \equiv \frac{\partial r}{\partial \theta} =& \left[{\hat x}^2 + \left(\frac{\hat y}{1-e}\right)^2\right]^{-1/2} \frac{1}{2}\left[2{\hat x}\frac{\partial {\hat x}}{\partial \theta} + \frac{2{\hat y}}{(1-e)^2} \frac{\partial {\hat y}}{\partial \theta}\right]\ \nonumber\\
 =& \frac{{\hat x}{\hat y}}{r}\left[ 1 - \frac{1}{(1-e)^2}\right]\,, \\
 %
  \partial_{e} r \equiv \frac{\partial r}{\partial e} =& \left[{\hat x}^2 + \left(\frac{\hat y}{1-e}\right)^2\right]^{-1/2} \frac{1}{2} \cdot \frac{2{\hat y}^2}{(1-e)^3}\,. \nonumber\\
 =& \frac{{\hat y}^2}{r\cdot(1-e)^3}
\end{align}
%
However, the usual approach in \textsc{PampelMuse} is that we do not fit source coordinates directly, but that we assume they are already known in some reference frame and the task is to optimize the coordinate transformation from the reference system into the actual system of the datacube. Starting from the reference coordinates ($u$, $v$), we obtain the actual coordinates via
%
\begin{align} 
 x^n = A\,u^n + B\,v^n + C \,,  
 \label{eq:coordtransX} \\
%
 y^n = A\,v^n - B\,u^n + D \,. 
 \label{eq:coordtransY}
\end{align}
%
In this case, $r$ is a function of $6$ potentially free parameters: $A$, $B$, $C$, $D$, $\theta$, and $e$. Accordingly, the partial derivatives $\partial_{x^n}r$ and $\partial_{y_n}r$ must be replaced by the partial derivatives with respect to $A$--$D$,
%
\begin{align}
 \partial_{A} r \equiv \frac{\partial r}{\partial A} =& \partial_{x^n} r \cdot\frac{\partial x^n}{\partial A} + \partial_{y^n} r \cdot\frac{\partial y^n}{\partial A} = \partial_{x^n} r \cdot u^n + \partial_{y^n} r \cdot v^n \nonumber \\
 =& \frac{1}{r}\left[ {\hat x}\cdot(v^n\cos\theta-u^n\sin\theta) - \frac{{\hat y}\cdot(u^n\cos\theta + v^n\sin\theta)}{(1-e)^2} \right]\,,\\
 %
 \partial_{B} r \equiv \frac{\partial r}{\partial B} =& \partial_{x^n} r \cdot\frac{\partial x^n}{\partial B} + \partial_{y^n} r \cdot\frac{\partial y^n}{\partial B} = \partial_{x^n} r \cdot v^n - \partial_{y^n} r \cdot u^n \nonumber \\
 =& -\frac{1}{r}\left[ {\hat x}\cdot(u^n\cos\theta+v^n\sin\theta) + \frac{{\hat y}\cdot(v^n\cos\theta - u^n\sin\theta)}{(1-e)^2} \right]\,,\\
 %
 \partial_{C} r \equiv \frac{\partial r}{\partial C} =& \partial_{x^n} r \cdot\frac{\partial x^n}{\partial C}  = \partial_{x^n} r\,,\\
% =& -\frac{1}{r}\left[ {\hat x}\sin\theta  + \frac{{\hat y}\cos\theta}{(1-e)^2}\right]\,,\\
 %
  \partial_{D} r \equiv \frac{\partial r}{\partial D} =& \partial_{y^n} r \cdot\frac{\partial y^n}{\partial D}  = \partial_{y^n} r\,.
% =& \frac{1}{r}\left[ {\hat x}\cos\theta  - \frac{{\hat y}\sin\theta}{(1-e)^2}\right]\,.
\end{align}

\subsection{The Gaussian profile}
\label{sec:gauss}

The functional form of the Gaussian profile as it is used in \textsc{PampelMuse} is
%
\begin{equation}
 G(r, e, \sigma, I) = \frac{I}{2\pi\sigma^2(1-e)}\exp\left\{\frac{-r^2}{2\sigma^2}\right\}\,.
\end{equation}
%
Note, however, that instead of the standard deviation $\sigma$, we use the $\fwhm$ to control the width of the Gaussian profile. The conversion between the two quantities is given as
%
\begin{equation}
 \fwhm = \sqrt{8\cdot \ln 2}\cdot\sigma\,.
\end{equation}
%
Furthermore, we control the integrated intensity $I$ of the Gaussian (and any other) profile by providing a magnitude, i.e.,
%
\begin{equation} \label{eq:intensity}
 I = 10^{-0.4\cdot m}\,.  
\end{equation}

The partial derivatives with respect to the individual parameters that define the shape of the profile ($\fwhm$, $\sigma$, and $\theta$) are calculated using the following formulae:
%
\begin{align}
 \partial_\fwhm G &= \partial_\sigma G/\sqrt{8\cdot \ln 2} \nonumber\\
 &= \exp\left\{\frac{-r^2}{2\sigma^2}\right\} \left(\frac{-I}{\pi\sigma^3(1-e)} + \frac{I\cdot r^2}{2\pi\sigma^5(1-e)}\right)/\sqrt{8\cdot \ln 2} \nonumber \\
 &= G \left(\frac{r^2}{\sigma^3} - \frac{2}{\sigma}\right)/\sqrt{8\cdot \ln 2}\,,\\
 %
 \partial_e G &= \exp\left\{\frac{-r^2}{2\sigma^2}\right\}\left(\frac{I}{2\pi\sigma^2(1-e)^2} - \frac{I\cdot r\cdot\partial_e r}{2\pi\sigma^4(1-e)}\right) \nonumber\\
 &= G \left( \frac{1}{1-e} - \frac{r\cdot\partial_e r}{\sigma^2} \right)\,,\\
 %
 \partial_\theta G &= \frac{I}{2\pi\sigma^2(1-e)}\exp\left\{\frac{-r^2}{2\sigma^2}\right\}\cdot\frac{-r\cdot\partial_\theta r}{\sigma^2} \nonumber\\
 &= -G\cdot\frac{r\cdot\partial_\theta r}{\sigma^2}\,,
\end{align}
%
where $\partial_e r$ and $\partial_\theta r$ are given as defined in Sect.~\ref{sec:positions}.

Regarding the partial derivatives with respect to any parameter connected to the centroid of the profile, the  corresponding formulae given in Sect.~\ref{sec:positions} are modified by a common factor,
%
\begin{align}
 \partial_\nu G &= \frac{I}{2\pi\sigma^2(1-e)}\exp\left\{\frac{-r^2}{2\sigma^2}\right\}\cdot\frac{-r\cdot\partial_\nu r}{\sigma^2} \nonumber\\
 &= -G\cdot\frac{r\cdot\partial_\nu r}{\sigma^2}\,,
\end{align}
%
with $\nu \in \{x^n, y^n, A, B, C, D\}$.

\subsection{The Moffat profile}
\label{sec:moffat}

The functional form of the Moffat profile that is included in \textsc{PampelMuse} is given as
%
\begin{equation} \label{eq:moffat}
 M(r, e, \rd, \beta, I) = \frac{I\cdot(\beta-1)}{\pi \rd^2\cdot(1-e)}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-\beta}\,,
\end{equation}
%
where the width of the profile is mainly defined by the effective radius $\rd$ and $\beta$ controls the kurtosis of the profile. The integrated intensity $I$ is the same as defined in Eq.~\ref{eq:intensity}. Similar to our approach for the Gaussian profile, we rather work with the $\fwhm$ instead of the effective radius $\rd$. The $\fwhm$ of the Moffat profile can be calculated via
%
\begin{equation} \label{eq:moffat_fwhm}
 \fwhm = 2\sqrt{2^{1/\beta}-1}\cdot \rd\,.
\end{equation}
%
Equation~\ref{eq:moffat_fwhm} implies that the $\fwhm$ is a function of both, $\rd$ and $\beta$. It should not be mixed up with the $\fwhm$ of a Gaussian profile that is a measure for the seeing during the observations. In general, the $\fwhm$ of a Moffat profile will be smaller than the seeing. This can be verified from Fig.~\ref{fig:fwhm_ratio_gauss_moffat}, where we have plotted the $\fwhm$ ratio between a Gaussian and a Moffat profile as a function of $\beta$. The plotted ratios were obtained by fitting a Gaussian profile to the flux distribution created by a Moffat profile with given $\fwhm$ and $\beta$. Figure~\ref{fig:fwhm_ratio_gauss_moffat} shows that the shape of the Moffat profile approaches the shape of  the Gaussian profile as $\beta$ increases. Larger $\beta$ values correspond to profiles with less pronounced wings. However, the Moffat profile will always have more pronounced wings than the Gaussian.

\begin{figure}[!ht]
 \centering\includegraphics[width=7cm]{fwhm_ratio_gauss_moffat.png}
 \caption{Inverse ratio between the $\fwhm$ of a Moffat profile and the $\fwhm$ of its best Gaussian fit, as a function of the $\beta$ value of the Moffat profile and its S/N.}
 \label{fig:fwhm_ratio_gauss_moffat}
\end{figure}

When calculating the partial derivatives of Eq.~\ref{eq:moffat}, we have to keep in mind that $\rd = \rd(\beta,\fwhm)$, and therefore
%
\begin{align}
\partial_\beta \rd &= \frac{\fwhm}{2} \cdot \frac{-1}{2} \cdot (2^{1/\beta}-1)^{-3/2} \cdot \frac{2^{1/\beta}\cdot \ln 2}{\beta^2} \nonumber\\
 &= \frac{\rd\cdot\ln 2}{2\cdot\beta^2\cdot(1-2^{1/\beta})} \\
\partial_\fwhm \rd &= \frac{1}{2\sqrt{2^{1/\beta}-1}}\,.
\end{align}
%
With respect to the shape parameters $\fwhm$, $\beta$, $\theta$ and $e$ we thus obtain the partial derivatives
%
\begin{align}
 \partial_\fwhm M &= \frac{-2I\cdot(\beta-1)}{\pi\rd^3\cdot(1-e)}\cdot \partial_\fwhm \rd \left[1+\left(\frac{r}{\rd}\right)^2\right]^{-\beta} \nonumber \\
 &\qquad - \frac{I\cdot\beta\cdot(\beta-1)}{\pi\rd^2\cdot(1-e)}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-\beta-1}\frac{-2r^2}{\rd^3} \partial_\fwhm \rd \nonumber \\
 &= \frac{2M}{\fwhm} \left[\beta\left(\frac{r}{\rd}\right)^2\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-1}-1\right]\,,\\
%
 \partial_\beta M &= \left(\frac{I}{\pi \rd^2\cdot(1-e)} - \frac{2I\cdot(\beta-1)}{\pi\rd^3\cdot(1-e)}\cdot \partial_\beta \rd\right) \left[1+\left(\frac{r}{\rd}\right)^2\right]^{-\beta} \nonumber\\
 &\qquad - \frac{I\cdot(\beta-1)}{\pi \rd^2\cdot(1-e)}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-\beta}\left( \ln\left[1+\left(\frac{r}{\rd}\right)^2\right] +\frac{2^{1+1/\beta}\beta r^2}{\rd^3 \left[1+\left(\frac{r}{\rd}\right)^2\right]}\partial_\beta \rd \right) \nonumber \\
 &= M \left(\frac{1}{\beta-1} - \frac{2\partial_\beta\rd}{\rd} - \ln\left[1+\left(\frac{r}{\rd}\right)^2\right] - \frac{2^{1+1/\beta}\beta r^2}{\rd^3 \left[1+\left(\frac{r}{\rd}\right)^2\right]}\partial_\beta \rd \right)\nonumber\\
 &= M \left(\frac{1}{\beta-1} - \ln\left[1+\left(\frac{r}{\rd}\right)^2\right] - \frac{2\partial_\beta\rd}{\rd}\cdot\left[ 1 + \frac{2^{1/\beta}\beta\cdot\left(\frac{r}{\rd}\right)^2}{1+\left(\frac{r}{\rd}\right)^2}\right]\right)\,,\\
 %
 \partial_\theta M &= \frac{-\beta\cdot I\cdot(\beta-1)}{\pi \rd^2\cdot(1-e)}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-\beta-1}\frac{2r}{\rd^2}\cdot\partial_\theta r \nonumber \\
 &= -\frac{2M\cdot\beta\cdot r}{\rd^2}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-1}\cdot\partial_\theta r\,,\\
 %
 \partial_e M &= \frac{I\cdot(\beta-1)}{\pi \rd^2\cdot(1-e)^2}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-\beta} \nonumber\\
 &\qquad - \frac{\beta\cdot I\cdot(\beta-1)}{\pi \rd^2\cdot(1-e)}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-\beta-1}\frac{2r}{\rd^2}\cdot\partial_e r \nonumber \\
 &= M\left(\frac{1}{1-e} - \frac{2\beta\cdot r\cdot\partial_e r}{\rd^2}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-1} \right)\,.
\end{align}

Just like in the case of the Gaussian profile, the partial derivatives with respect to the parameters that define the centroid of a profile can be obtained from the formulae in Sect.~\ref{sec:positions} via multiplication with a constant factor:
%
\begin{align}
  \partial_\nu M &= \frac{-\beta\cdot I\cdot(\beta-1)}{\pi \rd^2\cdot(1-e)}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-\beta-1}\frac{2r}{\rd^2}\cdot\partial_\nu r \nonumber \\
 &= -\frac{2M\cdot\beta\cdot r}{\rd^2}\left[1+\left(\frac{r}{\rd}\right)^2\right]^{-1}\cdot  \partial_\nu r\,,
\end{align}
where again $\nu \in \{x^n, y^n, A, B, C, D\}$.

\end{document}
