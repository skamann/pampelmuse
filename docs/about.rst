

About
=====

PampelMuse is a software package with the purpose of facilitating the analysis of integral-field spectroscopic
observations of crowded stellar fields. It provides several subroutines to perform the individual steps of the data
analysis. The theoretical work that went into the development of this code is described in detail in
[Kamann2013]_ and not repeated here. Instead, the purpose of this document is to provide a practical
guide on the analysis of IFS data with PampelMuse.