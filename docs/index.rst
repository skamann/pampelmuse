.. PampelMuse documentation master file, created by
   sphinx-quickstart on Fri Jul 24 11:08:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PampelMuse's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   start
   configuration
   data
   psf
   coordinates
   troubleshooting
   gui
   routines
   output
   contact
   references


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


