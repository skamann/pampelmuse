"""
source_selector.py
==================

Copyright 2013-2024 Sebastian Kamann

This file is part of PampelMuse.

PampelMuse is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

PampelMuse is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PampelMuse. If not, see <http://www.gnu.org/licenses/>.


+++
Purpose
-------
This module provides the class SourceSelector that is designed to
facilitate the process of selecting a useful subset of all sources
available in a photometric catalog for the analysis with PampelMuse.

Added
-----
2016/08/09, rev. 353

Last updated
------------
2024/09/13
"""
import logging
import sys
import warnings
from multiprocessing import cpu_count, Pool
import numpy as np
import pandas as pd
from astropy.wcs import WCS
from .aperture import AperPhoto
from .background import BackgroundGrid
from .coordinates import Transformation
from .sources import Sources
from ..instruments import Instrument, GenericIFS, Muse, MusePixtable
from ..psf import Variables
from ..psf.profiles import *
from ..utils.image import find_offset
from ..utils.statistics import der_snr


__author__ = 'Sebastian Kamann (s.kamann@ljmu.ac.uk)'
__revision__ = 20240913


logger = logging.getLogger(__name__)


def init_worker(variance, *args):
    """
    This function has the purpose of defining a global variable pointing
    to the variances of the IFS data. This is done in order to reduce the
    overhead of the initialisation of the parallel computation of the S/N
    estimates.

    Parameters
    ----------
    variance : nd_array
        The variances of the individual pixels of the data.
    args
    """
    global g_variance
    g_variance = variance.flatten()


def image_from_psf(sources_data):
    """
    Create a model image from a PSF profile and a set of centroid coordinates
    and magnitudes.

    Parameters
    ----------
    sources_data : dict
        The dictionary must contain the following two keys:
        - psf_instances : an instance of a PSF class
              The PSF profile is required to determine the flux distribution
              across the FoV
        - data: pandas.DataFrame
              The centroid coordinates and the magnitudes of the individual
              sources, provided as columns 'x', 'y', and 'mag'
        - offset : tuple, optional
              A global offset applied to all sources along x and y. Default is
              (0, 0)
        - zeropoint : float
              The magnitude zeropoint, i.e. the magnitude of a source with an
              integrated flux of 1. Default is 0.

    Returns
    -------
    image : nd_array
        The model image created from the input data.
    """
    psf = sources_data['psf']
    image = np.zeros((psf.n_pixel, ), dtype=np.float64)

    offset = sources_data.get('offset', (0., 0.))
    zeropoint = sources_data.get('zeropoint', 0.)

    n_sources = sources_data['data'].shape[0]
    for i, (index, row) in enumerate(sources_data['data'].iterrows(), start=1):
        psf.uc = row['x'] - offset[0]
        psf.vc = row['y'] - offset[1]
        psf.mag = row['mag'] - zeropoint
        psf.reset()

        image[psf.used] += psf.f
        logger.debug('Image creation: Processed star #{0} {1}/{2}'.format(index, i, n_sources))

    return image


def estimate_snr(sources_data):
    """
    Calculate the S/N ratio of all stars in the scene.

    Parameters
    ----------
    sources_data : dict
        The dictionary must contain the following keys:
        - psf_instances : an instance of a PSF class
              The PSF profile is required to determine the flux distribution
              across the FoV
        - data: pandas.DataFrame
              The centroid coordinates and the magnitudes of the individual
              sources, provided as columns 'x', 'y', and 'mag'
        - zeropoint : float
              The magnitude zeropoint, i.e. the magnitude of a source with an
              integrated flux of 1. Default is 0.

    Returns
    -------
    snratios : pandas.Series
        A pandas Series containing the S/N-ratio for each source
    totfluxes : float
        The integrated fluxes of all point sources over the individual
        spaxels.
    """
    global g_variance

    snratios = pd.Series(0., index=sources_data['data'].index)
    totfluxes = 0.

    psf = sources_data['psf']
    zeropoint = sources_data.get('zeropoint', 0.)

    n_sources = sources_data['data'].shape[0]
    for i, (index, row) in enumerate(sources_data['data'].iterrows(), start=1):
        psf.uc = row['x']
        psf.vc = row['y']
        psf.mag = row['mag'] - zeropoint
        psf.reset()

        if (g_variance[psf.used] == 0).any():
            logger.error("Sigma image contains invalid pixels with values <=0.")

        # measure S/N
        snratios[index] = np.sqrt(np.nansum((psf.f / np.sqrt(g_variance[psf.used])) ** 2))

        logger.debug("Estimated S/N for source #{0} [{1}/{2}]: {3}.".format(index, i, n_sources, snratios[index]))

        # add the flux contribution of this source in the FoV
        totfluxes += psf.f.sum()

    return snratios, totfluxes


class SourceSelector(object):
    """
    This class is designed to perform the selection process for the sources
    that are used in the PampelMuse extraction process.

    The defined methods perform the individual steps of the source selection
    process and are used by the INITFIT routine of PampelMuse.
    """

    def __init__(self, photometry_file, ifs_data, passband="f814w", zeropoint=None, position_fit=2):
        """
        Initialize a new instance of the SourceSelector class.

        Parameters
        ----------
        photometry_file : str
            The name of the ascii-file containing the photometric reference
            catalog. It should be in a format that is supported by PampelMuse.
            Check the manual for details.
        ifs_data : an instance of pampelmuse.instruments
            This instance should contain all the information about the IFS
            data that is being analysed.
        passband : str, optional
            The name of the photometric filter for which the source selection
            should be performed.
        zeropoint : float, optional
            An initial guess for the photometric zeropoint of the IFS data in
            the chosen passband.
        position_fit : int, optional
            A flag indicating how the source positions on the IFS data should
            be determined. Can be one of:
            0 - from a coordinate transformation with fixed parameters
            1 - from a coordinate transformation with fixed rotation and scale
                parameters but variable offsets.
            2 - from a coordinate transformation with variable rotation and
                scale parameters and variable offsets.
        """
        logger.info('Starting source selection based on {0}-magnitudes.'.format(passband.upper()))
        self.passband = passband
        self.zeropoint = zeropoint

        assert isinstance(ifs_data, Instrument), 'Parameter "ifs_data" muse be instance of an Instrument-class.'
        self.ifs_data = ifs_data
        self.use_wcs = self._check_wcs()

        # get results from crowded-field photometry, initialize sources
        logger.info('Reading photometry results from file "{0}".'.format(photometry_file))

        # open reference catalog. It should be in plain csv-format. the source IDs are used as index
        self.cfp_results = pd.read_csv(photometry_file)
        try:
            self.cfp_results.set_index('id', inplace=True)
        except KeyError:
            logger.critical("Did not find IDs in photometry file.")
            raise IOError("Source IDs must be provided with photometry.")
        finally:
            logger.info('Found the following columns: {0}'.format(', '.join([c for c in self.cfp_results.columns])))

        # make sure magnitudes are available
        if self.passband not in self.cfp_results.columns and self.passband.lower() not in self.cfp_results.columns:
            logger.critical("Did not find magnitudes in photometry file.")
            raise IOError("Source magnitudes must be provided with photometry.")
        elif self.passband in self.cfp_results.columns:
            # rename columns with magnitudes to 'mag'
            self.cfp_results.rename(columns={self.passband: 'mag'}, inplace=True)
        else:
            self.cfp_results.rename(columns={self.passband.lower(): 'mag'}, inplace=True)

        # make sure either RA & Dec or x & y are available; if both are available, RA & Dec are preferred over x & y
        if "ra" in self.cfp_results.columns and "dec" in self.cfp_results.columns:
            logger.info("Using RA & DEC coordinates as input.")
            ra = np.asarray(self.cfp_results["ra"], dtype=np.float64)
            dec = np.asarray(self.cfp_results["dec"], dtype=np.float64)

            # if we have a WCS instance, use it to convert to pixel coordinates
            if self.use_wcs:
                # get IFS spaxel coordinates from reference RA and DEC
                if self.ifs_data.wcs.wcs.naxis == 3:
                    x, y, _ = self.ifs_data.wcs.wcs_world2pix(ra, dec, np.ones(ra.shape), 1)
                else:
                    x, y = self.ifs_data.wcs.wcs_world2pix(ra, dec, 1)

            # otherwise create mock WCS instance with pixelsize 0.2arcsec
            else:
                wcs = WCS(naxis=2)
                wcs.wcs.ctype = ['RA---TAN', 'DEC--TAN']
                wcs.wcs.cdelt = [-0.2/3600., 0.2/3600.]
                wcs.wcs.crval = [ra.max(), dec.min()]
                wcs.wcs.crpix = [0, 0]
                x, y = wcs.wcs_world2pix(ra, dec, 1)

            self.cfp_results["x"] = x.tolist()
            self.cfp_results["y"] = y.tolist()

        elif "x" in self.cfp_results.columns or "y" in self.cfp_results.columns:
            self.use_wcs = False
            logger.info("using x- and-coordinates as input.")

        else:
            logger.critical("Cannot find input source coordinates.")
            raise IOError("Either x & y or RA & Dec must be provided with photometry.")

        # Perform sanity checks on provided source list (exclude sources with magnitudes NaN or >99):
        self.cfp_results['valid'] = np.isfinite(self.cfp_results['mag'])
        self.cfp_results.loc[abs(self.cfp_results['mag']) >= 99, 'valid'] = False
        if not self.cfp_results['valid'].all():
            logger.warning("Ignoring {0} source(s) with undefined magnitudes.".format(
                (~self.cfp_results['valid']).sum()))

        # check for duplicate indices (= source IDs)
        if self.cfp_results.index.duplicated().any():
            logger.error('Catalog contains duplicate source IDs.')

        # unless already present, add columns for storing status of a source and whether it is in a combined
        # component with a different primary source
        if 'status' not in self.cfp_results.columns:
            self.cfp_results['status'] = -1
        if 'primary' not in self.cfp_results.columns:
            self.cfp_results['primary'] = 0

        self.position_fit = position_fit
        self.sources = None
        self.psf_class = None
        self.psf_attributes = None
        self.identification_image = None
        self.data_image = None
        self.ccxoff = 0.0
        self.ccyoff = 0.0
        self.signal_to_noise = None

    def _check_wcs(self):
        """
        Internal method that checks if all necessary WCS information is
        available for the IFS data to perform the source selection based on
        RA and Dec instead of pixel coordinates.

        Returns
        -------
        use_wcs : bool
            A flag indicating if enough WCS information is available for the
            IFS data.
        """
        if self.ifs_data.wcs is None:
            return False
        elif self.ifs_data.wcs.naxis < 2:
            return False
        elif (self.ifs_data.wcs.wcs.crval[:2] == 0).any():
            return False
        else:
            try:
                return np.linalg.det(self.ifs_data.wcs.wcs.cd[:2, :2]) != 1.0
            except AttributeError:
                return (self.ifs_data.wcs.wcs.cdelt[:2] != 0).all()

    def background_setup(self, skysize=-1, slices=False, oversampling=1):
        """
        Method to add one or more background components.

        One background component corresponds to a constant background across
        the field of view. If more than one background component should be
        used, the method will set up a grid. The background in each pixel will
        then be obtained by interpolation of the grid points.

        Parameters
        ----------
        skysize : int, optional
            The size of each cell of the background grid in pixels. Set to -1
            for a constant background across the field of view.
        slices : boolean, optional
            Set this flag to create a separate background component for each
            IFU/slice. This is only used for MUSE data.
        oversampling : int, optional
            To improve the sky subtraction for MUSE pixel tables, the spectral
            sampling of the background components can be increased by the
            provided factor.

        Returns
        -------
        n_background : int, optional
            The number of background components used.
        """
        if skysize <= 0:
            background = BackgroundGrid.constant(oversampling=oversampling)
        else:
            background = BackgroundGrid.from_shape(shape=self.ifs_data.shape, rscale=skysize/2.,
                                                   oversampling=oversampling)

        if slices:
            logger.warning('Assigning background components to slices still in testing phase.')
            if isinstance(self.ifs_data, MusePixtable):
                if self.ifs_data.ifu_hdu is not None and self.ifs_data.slice_hdu is not None:
                    background.add_slices(n_ifu=24, n_slice=48)
                else:
                    logger.error('Need extensions "ifu" and "slice" for assigning background components to slices.')
            else:
                logger.error('Assigning background components to slices only supported for MUSE pixel tables.')

        # create data frame for background data
        n_centroids = len(background.centroids)
        index = pd.Index(np.arange(-1, -n_centroids-1, -1))  # sky components have negative index
        background_data = pd.DataFrame(True, columns=['valid', ], index=index)
        background_data['x'] = background.centroids[:, 0]
        background_data['y'] = background.centroids[:, 1]
        background_data['status'] = 4

        # change status of slice-only backgrounds (if any) to 5
        if slices:
            last_indices = background_data.tail(background.n_ifu*background.n_slice).index
            background_data.loc[last_indices, 'status'] = 5
        background_data['primary'] = index

        # ignore sky components outside FoV (if any)
        if skysize > 0:
            edge_distance = self.ifs_data.distance_to_edge(np.array([background_data['y'], background_data['x']]).T)
            outside_fov = edge_distance < -skysize
            logger.info('Removing {0}/{1} background components that do not contribute to observed flux.'.format(
                outside_fov.sum(), n_centroids))
            logger.debug('Centroids of removed background components: {0}'.format(
                background_data.loc[outside_fov, ['x', 'y']]))
            background_data.drop(index=background_data.index[outside_fov], inplace=True)
            n_centroids = (~outside_fov).sum()

        # attach background components to source catalog
        self.cfp_results = pd.concat([self.cfp_results, background_data])

        return n_centroids

    def make_sources_instance(self):
        """
        Method to create an instance of pampelmuse.core.Sources containing
        the current list of stellar and background sources.

        The method will run the initialisation method of the Sources-class and
        copy the dispersion information from the IFs data to the new instance.
        Afterwards, the instance can be accessed as SourceSelector().sources.
        """
        self.sources = Sources(catalog=self.cfp_results.drop(index=self.cfp_results.index[~self.cfp_results['valid']]))

        # unless a MUSE pixtable is being analysed, copy dispersion information to sources class
        if not isinstance(self.ifs_data, MusePixtable):
            if self.ifs_data.wcs.naxis > 2:
                self.sources.crval = self.ifs_data.wcs.wcs.crval[2]
                self.sources.crpix = self.ifs_data.wcs.wcs.crpix[2]
                self.sources.cdelt = self.ifs_data.wcs.pixel_scale_matrix[-1, -1]
                self.sources.ctype = self.ifs_data.wcs.wcs.ctype[2]
                self.sources.cunit = self.ifs_data.wcs.wcs.cunit[2].to_string()
            else:
                self.sources.crval = self.ifs_data.wave[0]
                self.sources.crpix = 1
                self.sources.cdelt = np.diff(self.ifs_data.wave).mean()
        else:
            self.sources.crval = self.ifs_data.wave[0]
            self.sources.crpix = 1
            self.sources.cdelt = np.diff(self.ifs_data.wave).mean()

        # make sure instance has correct dispersion information
        self.sources.n_dispersion = self.ifs_data.nlayer

    def prepare_psf(self, profile='moffat', parameters=None, use_lut=False, lut_radius=10):
        """
        Method to initialize the PSF that should be used in the source
        selection process.

        The method will define two properties, one containing the analytical
        profile that should be used to model the PSF (accessible afterwards as
        SourceSelector().psf_class) and one containing the values of the
        individual parameters that define the profile (accessible afterwards
        as SourceSelector().psf_attributes).

        Parameters
        ----------
        profile : str, optional
            The name of the analytical profile used to model the PSF.
            Currently 'moffat' (default), 'gauss', and 'double_gauss' are
            supported.
        parameters : iterable, optional
            The values of the parameters of the chosen profile, provided as a
            list that contains one dictionary per parameter. Each dictionary
            should contain three keys:
            - 'name': The name of the parameter
            - 'value': The (initial) value of the parameter
            - 'free': Boolean flag indicating if the parameter value should be
                      optimized in the analysis.
            By default, a round Moffat profile with beta=2.5 and fwhm=2.5 pix
            is used.
        use_lut : boolean, optional
            Flag indicating if the fluxes derived from the analytical PSF
            profile should be corrected using a 1dim. look-up table.
        lut_radius : float, optional
            The radius out to which the look-up table should be calculated, in
            pixels.
        """
        if profile == "moffat":
            self.psf_class = Moffat
        elif profile == "gauss":
            self.psf_class = Gaussian
        elif profile == "double_gauss":
            self.psf_class = DoubleGaussian
        elif profile == "moffat_gauss":
            self.psf_class = MoffatGaussian
        elif profile == "double_moffat":
            self.psf_class = DoubleMoffat
        elif profile == 'maoppy':
            self.psf_class = Maoppy
        else:
            logger.error('Unknown PSF type: "{0}"'.format(profile))
            return

        # prepare data and variability flags for default PSF
        n_parameters = len(Variables.PROFILES[profile])
        data = pd.DataFrame(np.zeros((1, n_parameters), dtype=np.float32), columns=Variables.PROFILES[profile])
        free = pd.Series(0, index=Variables.PROFILES[profile])

        # update PSF data and variability flags with user-provided data (if any)
        if parameters is not None:
            for p in parameters:
                data[p['name']] = p['value']
                free[p['name']] = p['free']
        # otherwise make sure fwhm (and beta) are >0
        else:
            data['fwhm'] = 2.5
            if 'beta' in Variables.PROFILES[profile]:
                data['beta'] = 2.5

        # initialise look-up table if requested
        if use_lut:
            lut = pd.DataFrame(1., index=data.index, columns=np.arange(lut_radius))
        else:
            lut = None

        # initialise instance with PSF parameters and print basic info about PSF
        self.psf_attributes = Variables(profile=profile, data=data, free=free, lut=lut)
        self.psf_attributes.info()

    def create_mock_image(self, n_stars=500, n_cpu=1):
        """
        Method to create a mock image from the catalogue of sources.

        This method creates a 2-dimensional image using the coordinates and
        magnitudes for the available sources and the current PSF model. The
        dimensions of the image are chosen so that all available sources are
        included. For large catalogs, the parameter 'n_stars' makes sure that
        the computation times stay moderate by only adding the subset of the
        brightest stars to the image. After creation, the image is available
        as SourceSelector().identification_image.

        The image creation is parallelized, and uses the `image_from_psf`
        function defined in this package.

        Parameters
        ----------
        n_stars : int, optional
            The maximum number of sources that is included in the image. The
            selection of sources is based on magnitudes (bright to faint).
        n_cpu : int, optional
            The number of CPUs used to prepare the image.
        """
        logger.info("Creating mock image from photometry catalogue...")

        if self.sources is None:
            logger.warning('Need to initialize instance of Sources-class first.')
            self.make_sources_instance()

        if self.psf_class is None:
            logger.warning('Need to initialize PSF first.')
            self.prepare_psf()

        shape = (int(self.sources.y.max() - self.sources.y.min()),
                 int(self.sources.x.max() - self.sources.x.min()))

        # initialize single PSF instance per CPU
        logger.info("   adding sources, working on {0} CPUs...".format(n_cpu))
        if n_cpu is None or n_cpu == -1:
            n_cpu = cpu_count()  # use all available CPUs

        # collect PSF parameters in dictionary for profile initialisation
        psf_parameters = {}
        for name in self.psf_attributes.names:
            psf_parameters[name] = self.psf_attributes.data.at[0, (name, 'value')]

        # select n_stars brightest sources - note that the selection has no effect on argsort because
        # the background components are at the end of the array
        used_ids = self.sources.mag[~self.sources.is_background].sort_values().index[:n_stars]
        to_cpu = np.arange(used_ids.size) % n_cpu

        # to get reasonable count rates in mock image, adjust zeropoint unless known
        zeropoint = self.sources.mag[used_ids[-1]] + 7.5 if self.zeropoint is None else self.zeropoint

        args = []
        psf = None
        for n in range(n_cpu):
            used = used_ids[to_cpu == n]
            psf = self.psf_class.from_shape(shape=shape, uc=0., vc=0., mag=0., maxrad=20., **psf_parameters)
            args.append({'psf': psf, 'data': self.sources.catalog.loc[used, ['x', 'y', 'mag']],
                         'offset': (self.sources.x.min(), self.sources.y.min()), 'zeropoint': zeropoint})

        if n_cpu > 1:
            pool = Pool(n_cpu)
            _results = pool.map_async(image_from_psf, args)
            pool.close()
            pool.join()
            _image = np.sum(_results.get(), axis=0)
        else:
            _image = image_from_psf(args[0])

        # 3. resample image on regular (x, y) grid
        logger.info("   re-sampling image on regular 2D grid ...")
        self.identification_image = np.zeros(shape, dtype=np.float32)
        self.identification_image[psf.y.astype('<i4'), psf.x.astype('<i4')] = _image

    def create_image_from_ifs(self, n_bin=10, **kwargs):
        """
        This method creates a two-dimensional image from the IFS data.

        The image is created by integrating the IFS data over the passband in
        which the magnitudes were provided. For large data cubes this can be
        quite time-consuming. In those cases, consider using the parameter
        'n_bin' to reduce the computation time. Afterward, the image is
        accessible via SourceSelector().data_image. Note that the method will
        also create an image with the average variances, accessible via
        SourceSelector().var_image.

        Parameters
        ----------
        n_bin : int, optional
            The binning factor; if >1, only every n_bin'th layer will be used
            to create the image.
        kwargs
            Used as a container for deprecated keyword arguments.
        """
        if 'apply_variances' in kwargs.keys():
            warnings.warn('Use of the "apply_variances" keyword is deprecated and has no effect.')

        logger.info("Creating mock {0}-band image from IFS data...".format(self.passband))
        i_min, i_max, weights = self.ifs_data.weights_from_filtercurve(
            self.passband,  layer_range=(5, self.ifs_data.nlayer - 5))
        self.data_image = self.ifs_data.get_image(i_min=i_min, i_max=i_max, weights=weights, n_bin=n_bin,
                                         use_variances=False)

        # The variance values are valid for the broadband image and must be increased to be representative for a
        # typical layer. As the image was obtained by integrating over a filter curve, the sigma values are multiplied
        # by sqrt(N) where N is the effective width of the filter curve in units of the spectral sampling.
        # self.var_image *= weights[(np.arange(weights.size) % n_bin) == 0].sum()

        if self.data_image.mask is not None and self.data_image.mask.all():
            logger.error("{0}-image created from IFS data contains only NaNs.".format(self.passband))
            logger.error("Check overlap between {0} throughput and IFS wavelength coverage.".format(self.passband))

        # if a true 3D cube has been provided, try to determine edge of field of view as smallest polygon to includes
        # all valid spaxels.
        if self.data_image.mask is not None and self.data_image.mask.any() and isinstance(self.ifs_data, GenericIFS):
            self.ifs_data.edge = self.ifs_data.find_edge(self.data_image.mask)

    def cross_correlate_images(self, ccmaxoff=None, init_guess=None):
        """
        This method tries to measure the offset between the IFS data and the
        coordinates in the source catalogue.

        The method will cross-correlate the image created from the IFS_data
        with the mock image created from the source catalog and then find the
        peak intensity of the cross-correlation signal if a pre-defined area.
        The coordinates of this point are considered as the most-likely offset
        between the two images and are accessible via SourceSelector().ccxoff
        and SourceSelector().ccyoff afterwards.

        Parameters
        ----------
        ccmaxoff : float, optional
            The maximum allowed offset between the two images in pixels.
        init_guess : tuple, optional
            Initial guesses for the offset along the x- and y-axes. The peak
            in the cross-correlation signal is searched in a square with half-
            width 'ccmaxoff' centred on the initial guess. IMPORTANT: The
            offsets are measured relative to the initial guess coordinates.
        """

        if not self.use_wcs:
            logger.error('Cannot perform cross-correlation unless valid WCS instance is available.')
            return

        if self.identification_image is None:
            logger.warning('Need to create identification image for cross-correlation first.')
            self.create_mock_image()

        if self.data_image is None:
            logger.warning('Need to create image from IFS data for cross-correlation first.')
            self.create_image_from_ifs()

        if init_guess is None:  # need to correct for shift between image and catalogue.
            init_guess = (-self.sources.x.min(), -self.sources.y.min())

        if ccmaxoff > 0:
            ifs_image = self.data_image.data.copy()
            ifs_image[self.data_image.mask] -= 0

            self.ccxoff, self.ccyoff = find_offset(ifs_image, self.identification_image, max_off=ccmaxoff,
                                                   init_guess=init_guess)
            # Note that the offset are provided with negative signs because the cross-correlation routine determines
            # the offset from the reference image to the IFS data, while we need it vice versa.
            logger.info("Estimated pointing offset from catalogue: dx={0:.1f}, dy={1:.1f}".format(self.ccxoff,
                                                                                                  self.ccyoff))
        else:
            logger.warning("Skipping determination of offset between IFS data and catalogue.")

        # initialize/update coordinate transformation
        self.sources.transformation = Transformation(
            data=pd.DataFrame(np.array([[1., 0., 0., 1., -self.ccxoff, -self.ccyoff]], dtype=np.float32),
                              columns=Transformation.PARAMETERS))
        if self.position_fit == 2:
            self.sources.transformation.free[:] = True
        elif self.position_fit == 1:
            self.sources.transformation.free[-2:] = True

    def identify_footprint(self, n_stars=100):
        """
        This method allows for an interactive determination of the location of
        the IFS data relative to the source catalog.

        This method will open the IFS data and the mock image created from the
        source catalogue in the PampelMuse GUI. Then the locations of the
        individual sources will be overplotted on the mock image. If the
        cross-correlation via SourceSelector().cross_correlate_images() has
        been run successfully before, the method will also overplot the likely
        location of the IFS data on the mock image and the initial guesses of
        the sources on the IFS data. Note that this method requires PyQt4 to
        be installed on the system.

        Parameters
        ----------
        n_stars : int, optional
            The number of stars that are initially over-plotted on the mock
            image. Note that it will be possible to adjust this number once
            the GUI has been loaded.
        """
        from PyQt5 import QtWidgets
        from ..graphics.pampelmuse_gui import PampelMuseGui

        logger.info("Preparing GUI for source identification...")

        app = QtWidgets.QApplication(sys.argv)

        # get magnitude of n_stars'th brightest star
        n_stars = min(n_stars, self.sources.n_sources - self.sources.n_background - 1)
        mag = self.sources.mag[~self.sources.is_background].sort_values().iloc[n_stars]

        identifier = PampelMuseGui(sources=self.sources, ifsData=self.ifs_data, refImage=self.identification_image)

        # determine sizes for plotting sources
        sizes = 10. * (10 ** (-0.2 * (self.sources.mag - mag - 1.)))
        sizes[sizes <= 1] = 1
        identifier.sources.symbol_size = sizes

        # account for offset between reference image and true coordinates
        (dx, dy) = (self.sources.x.min(), self.sources.y.min())
        identifier.refOffsets = {'x': pd.Series(self.sources.x - dx, index=self.sources.ids),
                                 'y': pd.Series(self.sources.y - dy, index=self.sources.ids)}
        identifier.sources.plotted = self.sources.ids[self.sources.ids >= 0]
        identifier.plot_sources_on_ref()

        if self.use_wcs:
            # show approximate location of IFS field of view in reference view
            identifier.addFovToReference(self.ccxoff - self.sources.x.min() + self.ifs_data.shape[1]//2,
                                         self.ccyoff - self.sources.y.min() + self.ifs_data.shape[0]//2)

            # overplot positions of sources on IFS as determined via cross-correlation
            select = (self.sources.status < 4) & (
                self.sources.mag <= mag) & (
                self.ifs_data.infov(np.vstack((self.sources.x - self.ccxoff, self.sources.y - self.ccyoff)).T))
            identifier.ifsPlot.addData(self.sources.x[select] - self.ccxoff, self.sources.y[select] - self.ccyoff,
                                       s=sizes[select], marker="o", edgecolor="g", facecolor="None")

        identifier.show()
        app.exec_()

        identified = identifier.identifiedSources

        if len(identified) >= 4:
            # TODO: reduce number of required sources to 2 if only offsets should be determined

            # collect IFS coordinates in common DataFrame and fit coordinate transformation
            ifs_coordinates = pd.DataFrame(
                0., index=[self.sources.wave[0]], columns=pd.MultiIndex.from_product([identified.keys(), ['x', 'y']]))
            for source_id, values in identified.items():
                ifs_coordinates.at[self.sources.wave[0], (source_id, 'x')] = values[0]
                ifs_coordinates.at[self.sources.wave[0], (source_id, 'y')] = values[1]

            self.sources.transformation = Transformation.from_coordinates(self.sources.catalog, ifs_coordinates)

            if self.position_fit == 2:
                self.sources.transformation.free[:] = True
            elif self.position_fit == 1:
                self.sources.transformation.free[-2:] = True

        elif not self.sources.transformation.valid:
            logger.error("At least 4 sources must be identified.")

    def calibrate_photometry(self, n_cpu=1, update_zeropoint=True):
        """
        This method performs a photometric calibration of the IFS data along
        with an S/N estimation for all the sources in the catalog.

        The method will perform the following steps to estimate the average
        S/N of a source on the IFS data.
        1) Based on a hypothetical zeropoint (i.e. the one provided during the
           initialisation of the instance), the flux profiles of all sources
           are determined and a hypothetical S/N estimate is obtained. This
           step is parallelized and also yields an estimate of the total flux
           in the image.
        2) The integrated flux is compared to the sum of all pixel values in
           the broadband image created from the IFS data before. The ratio of
           these two fluxes can be converted into a magnitude difference
           between the hypothetical and the real zeropoint.
        3) All S/N estimates are corrected for this magnitude difference

        Parameters
        ----------
        n_cpu : int, optional
            the number of CPUs used in the parallelized part of the method.
        update_zeropoint : bool, optional
            Flag indicating if the zeropoint should be updated based on the
            flux comparison with the IFS data. If not, the S/N estimates of
            all sources are based on the zeropoint provided upon
            initialisation of the instance.
        """
        logger.info("Estimating S/N for all sources...")

        i_min, i_max, weights = self.ifs_data.weights_from_filtercurve(
            self.passband, layer_range=(5, self.ifs_data.nlayer - 5))
        # TODO: Need n_bin parameter?
        ifs_photometry = self.ifs_data.get_layer(i_min=i_min, i_max=i_max, weights=weights, n_bin=10)

        # As a last resort when no variances are available, estimate them using the standard deviation of the 50%
        # faintest pixels
        if 'variance' not in ifs_photometry.columns:
            logger.error("Variance values required for meaningful S/N estimates.")
            logger.warning("Obtaining crude variance values IFS data.")
            faint = ifs_photometry['value'] <= np.nanmedian(ifs_photometry['value'])
            std = np.nanstd(ifs_photometry.loc[faint, 'value'])
            ifs_photometry['variance'] = std**2

        # Workaround for data cubes with over-subtraction of background, resulting in negative integrated fluxes
        # This happened for some of the UFD cubes that were processed with advanced sky subtraction routines.
        if np.nansum(ifs_photometry['value']) <= 0:
            ifs_photometry['value'] -= np.nanmedian(ifs_photometry['value'])
            logger.warning('Flux sum in {0}-image was negative. Subtracted (negative) median flux.'.format(
                self.passband))
            if np.nansum(ifs_photometry['value']) <= 0:
                fixed_pixels = 0
                while np.nansum(ifs_photometry['value']) <= 0:
                    ifs_photometry.at[ifs_photometry['value'].idxmin(), 'value'] = 0
                    fixed_pixels += 1
                logger.warning(
                    "Flux sum in {0}-image was still negative. Set the {1} most negative pixels to zero.".format(
                        self.passband, fixed_pixels))

        # collect PSF parameters in dictionary for profile initialisation
        psf_parameters = {}
        for name in self.psf_attributes.names:
            psf_parameters[name] = self.psf_attributes.data.at[0, (name, 'value')]

        # add parameters of coordinate transformation
        for name in self.sources.transformation.names:
            psf_parameters[name] = self.sources.transformation.data[(name, 'value')].iloc[0]

        # create coordinate transformation for (valid pixels of) variance image
        transform = ifs_photometry[['y', 'x']].values

        # Assign sources to CPUs
        if n_cpu is None or n_cpu == -1:
            n_cpu = cpu_count()  # use all available CPUs
        to_cpu = np.arange(self.sources.n_sources) % n_cpu
        to_cpu[self.sources.ids < 0] = n_cpu + 1  # ignore background components
        args = []

        # handling of photometric zeropoint
        if self.zeropoint is None and not update_zeropoint:
            logger.error('Fixed photometric zeropoint requested but no value provided.')
        zeropoint = 25. if self.zeropoint is None else self.zeropoint

        # prepare PSF instances for all sources
        for n in range(n_cpu):
            psf = self.psf_class(transform=transform, uc=0., vc=0., mag=0., maxrad=10., **psf_parameters)
            args.append({'psf': psf, 'data': self.sources.catalog.loc[to_cpu == n, ['x', 'y', 'mag']],
                         'zeropoint': zeropoint})

        # estimate S/N and integrated flux from sources in image
        if n_cpu > 1:
            pool = Pool(n_cpu, initializer=init_worker, initargs=(ifs_photometry['variance'].values,))

            _results = pool.map_async(estimate_snr, args)
            pool.close()
            pool.join()
            results = _results.get()

            self.signal_to_noise = pd.concat([r[0] for r in results], verify_integrity=True)
            intflux = np.sum([r[1] for r in results])
        else:
            init_worker(*(ifs_photometry['variance'].values,))
            self.signal_to_noise, intflux = estimate_snr(args[0])

        # Calculate ratio between expected flux and actual flux in image to update zeropoint
        logger.info("Estimating the photometric zeropoint...")
        fratio = intflux/ifs_photometry['value'].sum()
        dmag = -2.5*np.log10(fratio)
        logger.info("Estimated photometric zeropoint ({0}) is {1:.2f}".format(self.passband, zeropoint + dmag))

        # unless zeropoint fixed, update value and S/N estimates
        if update_zeropoint:
            self.zeropoint = zeropoint + dmag
            self.signal_to_noise /= fratio
        else:
            logger.info('Difference to requested photometric zeropoint is {0:.2f} mag.'.format(dmag))

        logger.info('S/N ratios calculated based on photometric zeropoint of {0:.2f}.'.format(self.zeropoint))

    @property
    def fwhm(self):
        if self.psf_class is None:
            return np.nan
        elif self.psf_class in [DoubleGaussian, DoubleMoffat, MoffatGaussian, Maoppy]:
            # for combined profiles, use dedicated classmethod to determine FWHM of full profile
            args = {}
            for name in self.psf_attributes.names:
                if name == 'theta':
                    continue
                args[name] = np.nanmean(self.psf_attributes.data[(name, 'value')])
            return self.psf_class.calculate_fwhm(**args)
        else:
            # otherwise directly use FWHM parameter
            return np.nanmean(self.psf_attributes.data[('fwhm', 'value')])

    def select(self, reslim=None, sncut=3., confusion_density=0.4, binrad=0.2, use_unres=False, unres_range=2):
        """
        This method performs the source selection process for the PSF fitting,

        Each source is given a status based on criteria such as the estimated
        S/N, the distance to brighter sources, and the edge of the field of
        view. The status of a source can be one of the following:
            4 - a background/sky spectrum
            2 - a resolved source for which a spectrum will be extracted
            1 - a source that should be resolved according to its brightness,
                but is too close to a brighter source. Its contribution is
                added to the brighter source in a combined PSF.
            0 - an unresolved source that is included in the grainy stellar
                background component
           -1 - an unresolved source that is not considered in the fit.

        Parameters
        ----------
        reslim : float, optional
            If this parameter is set to a value different from None, the
            method will not try to determine the confusion limit itself but
            will only include sources brighter than the provided value.
        confusion_density : float, optional
            The source density where the confusion limit is reached, provided
            as [no. of sources]/[resolution element]. A value of 0.4 gave good
            results for the simulations performed in Kamann et al. (2013).
        sncut : float, optional
            The minimum estimated S/N of a source that is still considered as
            resolved.
        binrad : float, optional
            The minimum distance for which sources are considered resolved,
            provided relative to the FWHM of the PSF profile. Sources with
            mutual distances below this value will be combined in the
            analysis.
        use_unres : bool, optional
            Flag indicating if an unresolved component should be included in
            the selection.
        unres_range : float, optional
            If an unresolved component should be included, this parameter
            gives the range in magnitudes of the sources that are included in
            the component. Note that the bright end of this range is set to
            the magnitude of the faintest resolved source.
        """
        logger.info("Selecting sources to be used in PSF fitting...")

        if self.signal_to_noise is None:
            logger.warning('Need to estimate S/N for all sources before performing selection.')
            self.calibrate_photometry()

        if np.isnan(self.fwhm):
            logger.error('Need to define PSF before source selection can be performed.')
            return

        # for larger fields of view, such as the MUSE WFM, the stellar density can vary significantly across its extent.
        # To deal with this, the confusion limit is estimated locally in an area of radius 10xFWHM around each star
        density = np.zeros(self.ifs_data.nspaxel, dtype=np.int16)

        # load IFS coordinates for all stars (using first layer as reference)
        self.sources.load_ifs_coordinates(layers=0)
        #  get IFS coordinates as DataFrame with source IDs as indices and x & y as columns
        _x = self.sources.ifs_coordinates.loc[:, (slice(None), 'x', 'value')]
        _y = self.sources.ifs_coordinates.loc[:, (slice(None), 'y', 'value')]
        xy = pd.DataFrame(np.stack((_x.values[0], _y.values[0]), axis=1), index=_x.columns.levels[0],
                          columns=['x', 'y'])

        logger.info("       ID   mag    S/N  d_bright  d_edge  status")
        logger.info("                           [spx]   [spx]        ")

        # loop over magnitude-sorted list of indices of the sources the instance of the Sources-class
        # (brightest sources first)
        for source_id, mag in self.sources.mag.sort_values().items():

            if source_id < 0:  # ignore background components
                continue

            # stop loop if current source falls below (fixed) confusion limit
            if reslim is not None and mag > reslim:
                break

            # get x- and y-coordinate
            xi = xy.at[source_id, 'x']
            yi = xy.at[source_id, 'y']

            # get position of source relative to field of view
            distance_to_pixels = self.ifs_data.distance_to_spaxels(xi, yi)[..., 0]
            distance_to_edge = self.ifs_data.distance_to_edge([[xi, yi]])[0]

            # check if local confusion limit is reached
            slc = distance_to_pixels < (10.*self.fwhm)
            if slc.any():
                # no. of resolution elements is [no. of pixels]/[pixels per resolution element]
                n_resolution = (slc.sum()/(np.pi*(0.5*self.fwhm)**2))

                # confusion limit is given as [no. of sources]/[no. of resolution elements]
                if density[slc].sum()/n_resolution > confusion_density:
                    continue

            # get S/N estimate for the source
            snr = self.signal_to_noise[source_id]
            if snr < sncut:
                continue

            # penalize S/N if source is outside FoV:
            if distance_to_edge < 0:
                snr *= np.exp(-(distance_to_edge/self.fwhm)**2)
                if snr < sncut:
                    continue
            # otherwise, add source to array for calculation of stellar density
            else:
                density[distance_to_pixels.argmin()] += 1

            # calculate distance to next brighter source
            brighter = self.cfp_results.index[self.cfp_results['status'] == 2]
            if brighter.any():
                dr = np.sqrt((xi - xy.loc[brighter, 'x'])**2 + (yi - xy.loc[brighter, 'y'])**2)
                id_near = dr.idxmin()

                # issue warning if two sources have basically the same IFS coordinates
                if dr[id_near] < 1e-5:
                    logger.warning("Source #{0} might be double.".format(source_id))

                # penalize S/N if a brighter source is located inside the FWHM of the PSF
                if dr[id_near] < self.fwhm:
                    snr *= (1.+0.9*np.log10(dr[id_near]/self.fwhm))

                    if dr[id_near] < (binrad*self.fwhm) or snr < sncut:
                        self.cfp_results.at[source_id, 'status'] = 1
                        self.cfp_results.at[source_id, 'primary'] = id_near
                        logger.info("{0:9d}  {1:4.1f}  {2:6.1f}  {3:8.2f}  {4:6.2f}       1".format(
                            source_id, mag, snr, dr[id_near], distance_to_edge))
                        continue

            self.cfp_results.at[source_id, 'status'] = 2
            self.cfp_results.at[source_id, 'primary'] = source_id
            logger.info("{0:9d}  {1:4.1f}  {2:6.1f}            {3:6.2f}       2".format(
                source_id, mag, snr, distance_to_edge))

        logger.info("Results of source selection: {0} resolved, {1} nearby sources.".format(
            np.sum(self.cfp_results['status'] == 2), np.sum(self.cfp_results['status'] == 1)))

        if not (self.cfp_results['status'] == 2).any():
            logger.error("Did not find any resolved sources. Aborting...")
            return

        # if requested, update confusion limit. Note that the configuration that is stored in the header of the
        # prm-file may contain two different values for "RESLIM": the initial one provided by the user and the one
        # just determined from the source list.
        if reslim is None:
            reslim = self.sources.mag[self.cfp_results.index[self.cfp_results['status'] == 2]].max()
            logger.info("Estimated confusion limit is m_{0}={1:.1f}".format(self.passband, reslim))

        # if requested, add a component of unresolved stars
        if use_unres:
            unreslim = reslim + unres_range

            logger.info("Including sources to magnitude m_{0}={1:.2f} to unresolved component...".format(
                self.passband, unreslim))

            for source_id, mag in self.sources.mag.sort_values().items():

                if self.cfp_results.at[source_id, 'status'] > 0:
                    continue

                if mag > unreslim:
                    break

                # get x- and y-coordinate
                xi = xy.at[source_id, 'x']
                yi = xy.at[source_id, 'y']

                distance_to_edge = self.ifs_data.distance_to_edge([[xi, yi]])[0]
                if distance_to_edge > -1:
                    self.cfp_results.at[source_id, 'status'] = 0

            # use brightest unresolved source as "primary"
            if (self.cfp_results['status'] == 0).sum() > 0:
                idx = self.sources.mag[self.cfp_results.index[self.cfp_results['status'] == 0]].idxmin()
                self.cfp_results.loc[self.cfp_results['status'] == 0, 'primary'] = idx

            logger.info("Unresolved component contains {0} sources.".format((self.cfp_results['status'] == 0).sum()))

        # set properties in sources class
        self.sources.primary = self.cfp_results.loc[self.sources.ids, 'primary']
        self.sources.status = self.cfp_results.loc[self.sources.ids, 'status']
        self.sources.load_spectra(init_wave=False)  # isinstance(self.ifs_data, MusePixtable)) #TODO: remove2

    def search_psf_stars(self, saturated=None, max_mag=None, psf_radius=10, psf_radius_scaling=None,
                         aperture_noise=0.1):
        """
        This method searches the photometry for stars suited to constrain
        the PSF.

        Stars that are considered suitable for constraining the PSF must
        fulfil the following criteria:
        (1) brighter than the provided magnitude limit (parameter max_mag)
        (2) must have been identified as resolved (i.e. status==2) in the
            source catalog
        (3) must not have close companions (sources with status=1)
        # (4) must have an estimated S/N above the provided cut (this is
        #     estimated using aperture photometry)
        # This criterion is currently not used.
        (5) at a distance of at least the PSF FWHM from the edge of the FoV
            (2 x FWHM for MUSE)
        (6) isolation criteria:
            a) relative contribution from neighboring sources inside the PSF
               radius should be smaller than limit given by 'aperture_noise'
               parameter
            b) no brighter source inside 1.5 x PSFRAD distance

        Note that the isolation of a star is measured using a slightly
        modified version of the shoulder drop or topographic prominence (for
        the exact definition of the topographic prominence, see
        http://en.wikipedia.org/wiki/Topographic_prominence). It is a measure
        for how strongly a source dominates its surroundings. The measure used
        here is the flux ratio of the source itself and the brightest source
        within a distance r that we set to the value of the PSF radius.

        Parameters
        ----------
        saturated : list, optional
            A list of source IDs that should be excluded from the PSF star
            search, e.g. because they are saturated in the IFS data.
        max_mag : float, optional
            The maximum allowed magnitude of a PSF star. If a negative value
            is provided, the maximum allowed magnitude is obtained by sub-
            tracting the value from the magnitude of the brightest star in
            the field of view.
        psf_radius : float, optional
            The radius out to which the PSF profiles are calculated. The
            isolation criterion will be checked in a circle with this radius.
        psf_radius_scaling : float <1, optional
            The PSF radius can be adapted based on the predicted flux of each
            star. If provided, 'psf_radius_scaling' defines the fractional
            flux (relative to the brightest star in the field of view) that
            corresponds to a reduction of psf_radius by a factor of 2.
        aperture_noise : float, optional
            The maximum allowed contamination of a nearby source inside the
            area where the PSF profile is defined.
        """
        logger.info("Searching for possible PSF stars...")

        if not (self.sources.status == 2).any():
            logger.error('Source selection must be performed before searching for PSF stars.')
            return

        if saturated is not None:
            logger.info("Ignoring following stars flagged as saturated: {0}".format(", ".join(
                [str(star) for star in saturated])))
        else:
            saturated = []

        # load IFS coordinates for all stars (again using first layer as reference)
        self.sources.load_ifs_coordinates(layers=0)
        #  get IFS coordinates as DataFrame with source IDs as indices and x & y as columns
        _x = self.sources.ifs_coordinates.loc[:, (slice(None), 'x', 'value')]
        _y = self.sources.ifs_coordinates.loc[:, (slice(None), 'y', 'value')]
        xy = pd.DataFrame(np.stack((_x.values[0], _y.values[0]), axis=1), index=_x.columns.levels[0],
                          columns=['x', 'y'])

        # measure distances of sources to edge of field of view
        distance_to_edge = pd.Series(self.ifs_data.distance_to_edge(xy.values), index=xy.index)

        # find magnitude of brightest star in field of view, adjust maximum allowed magnitude if requested
        min_mag = self.sources.mag[(self.sources.status == 2) & (distance_to_edge > 0)].min()
        if max_mag is not None and max_mag < 0:
            max_mag = min_mag - max_mag

        logger.info('IDs of stars that fulfill criteria for PSF stars:')
        logger.info('      ID|   mag| d_edge [pixel]| shoulder_drop ')
        logger.info('        +      +               + [apernois={0:.2f}]'.format(aperture_noise))
        logger.info('--------+------+---------------+---------------')

        # loop over magnitude-sorted list of indices of the sources the instance of the Sources-class
        # (brightest sources first)
        psf_indices = []
        for source_id, mag in self.sources.mag.sort_values().items():
            if max_mag is not None and mag > max_mag:
                break
            if self.sources.status[source_id] != 2:
                continue
            if source_id in saturated:
                continue
            if np.sum(self.sources.primary == source_id) != 1:
                continue

            if distance_to_edge[source_id] < self.fwhm:
                continue
            elif distance_to_edge[source_id] < 2*self.fwhm and isinstance(self.ifs_data, (Muse, MusePixtable)):
                continue

            # calculate shoulder drop
            dr = np.sqrt((xy.at[source_id, 'x'] - xy['x'])**2 + (xy.at[source_id, 'y'] - xy['y'])**2)
            dr[source_id] = np.inf  # handle the source itself
            dr[self.sources.status >= 4] = np.inf  # handle background components

            # check if PSF radius should be adapted based on predicted flux
            if psf_radius_scaling is not None:
                r_i = psf_radius*(2**(0.4*(mag - min_mag)/np.log10(psf_radius_scaling)))
            else:
                r_i = psf_radius

            if r_i < (0.5*self.fwhm):
                continue
            if dr.min() < r_i:
                dmag = (self.sources.mag[dr[dr < r_i].index] - mag).min()
                shoulder_drop = 10**(-0.4*dmag)
                if shoulder_drop > aperture_noise:
                    continue
            else:
                shoulder_drop = 0

            # check if brighter star inside 1.5x PSFRAD:
            if dr.min() < 1.5*r_i and self.sources.mag[dr[dr < 1.5*r_i].index].min() < mag:
                continue

            # self.sources.catalog.at[source_id, 'status'] = 3
            psf_indices.append(source_id)
            logger.info("{0:8d}|{1:6.2f}|{2:15.1f}|{3:15.2f}".format(source_id, mag, distance_to_edge[source_id],
                                                                     shoulder_drop))

        # For some reason I don't understand, the status of the PSF sources is not updated to 3 unless I reset the
        # type of the column
        self.sources.catalog['status'] = self.sources.catalog['status'].astype(int)
        self.sources.catalog.loc[psf_indices, 'status'] = 3

        if len(self.sources.psf_indices) == 0:
            logger.error("No possible PSF sources found!")
            return

    def run_aperture_photometry(self, ids=None, start=0, stop=-1, radius=3, n_cpu=1, n_bin=1):
        """
        Method to perform aperture photometry for a subset of the source
        catalog on the layers of the IFS data.

        Based on the estimates for the centroid coordinates in the IFS data,
        this method will obtain spectra for a given set of sources by
        performing aperture photometry for those sources on all layers of the
        IFs data. At the same time, the method will refine the centroids and
        also obtain estimates of the PSF parameters using the spatial moments
        measured during the analysis. The results will be written to the
        SourceSelector().sources and SourceSelector().psf_attributes
        properties.

        Parameters
        ----------
        ids : list, optional
            The IDs of the stars for which the analysis should be performed.
            If none are provided, the method will use all sources that have
            status=3 in the SourceSelector().sources property.
        start : int, optional
            The first layer of the IFS data for which the analysis should be
            performed.
        stop : int, optional
            The last layer of the IFS data for which the analysis should be
            performed.
        radius : int, optional
           The aperture radius inside which the flux and the moments of each
           source are determined.
        n_cpu : int optional
           The number of CPUs used in the parallelized part of the analysis.
        n_bin : int, optional
           The binning factor of the IFS data. If >1, one new layer is created
           by combining the signal from 'n_bin' adjacent layers.
        """
        # carry out aperture spectrophotometry for PSF sources
        logger.info("Initializing aperture spectrophotometry for possible PSF stars...")

        if ids is None:
            ids = self.sources.ids[self.sources.status == 3]
        else:  # make sure IDs are available in Sources instance
            ids = self.sources.ids[np.in1d(self.sources.ids, ids, assume_unique=True)]
        if len(ids) == 0:
            logger.error('Did not find any suitable sources for aperture photometry.')
            return

        # make sure sources instance has correct dispersion information
        if self.sources.n_dispersion != self.ifs_data.nlayer:
            logger.warning('Need to update dispersion information in source catalog.')
            self.sources.n_dispersion = self.ifs_data.nlayer

            # create dummy spectrum data: every spectrum contains only NaNs
            # if a MUSE pixtable is being analysed, a dedicated wavelength array must be created as well.
            self.sources.load_spectra(init_wave=False)  # isinstance(self.ifs_data, MusePixtable)) #TODO: remove
            self.sources.data[:] = np.nan

        # Obtain centroid coordinates for the selected sources
        self.sources.load_ifs_coordinates(ids=ids.values)

        #  get IFS coordinates as DataFrame with source IDs as indices and x & y as columns
        _x = self.sources.ifs_coordinates.loc[:, (slice(None), 'x', 'value')]
        _y = self.sources.ifs_coordinates.loc[:, (slice(None), 'y', 'value')]
        xy = pd.DataFrame(np.stack((_x.values[0], _y.values[0]), axis=1), index=_x.columns.levels[0],
                          columns=['x', 'y'])

        logger.info('Using following stars in aperture spectrophotometry:')
        logger.info('       ID      x      y')
        for source_id, row in xy.iterrows():
            logger.info('{0:9d}{1:7.1f}{2:7.1f}'.format(source_id, row['x'], row['y']))

        # initialize aperture routine
        aperture = AperPhoto(cube=self.ifs_data, centroids=xy)

        # run it - note that the array that is returned may contain NaN values (if the moment calculation failed or
        # lead to unphysical values).
        results, background = aperture(r_aper=radius, layer_range=(start, stop), background="local", n_cpu=n_cpu,
                                       n_bin=n_bin)

        # make sure no source returned only NaN
        mean_results = results.mean(axis=0)
        for source_id, values in mean_results.groupby(level=0):
            if np.isnan(values).any():
                logger.warning('Excluding source #{0} as calculation returned only NaNs.'.format(source_id))
                results.drop(source_id, axis=1, inplace=True)
                self.sources.status[source_id] = 2

        # initialize containers for storing profile shape parameters
        fwhm = []
        e = []
        theta = []

        # loop over sources, insert their spectra, calculate FWHM, ellipticity, and pos. angle of profile
        # note that removed source IDs are still in the columns property, see discussion here:
        # https://github.com/pandas-dev/pandas/issues/2770
        for source_id in np.unique(results.columns.get_level_values(0)):

            # get spectrum
            spectrum = results[(source_id, 'flux')]

            # add flux of source to Sources instance
            self.sources.data[(self.sources.specrow[source_id], 'flux')].iloc[results.index] = spectrum.values

            # add coordinates if they are fitted
            if self.sources.ifs_coordinates_free.any():
                self.sources.ifs_coordinates[source_id] = np.nan
                self.sources.ifs_coordinates[
                    (source_id, 'x', 'value')].iloc[results.index] = results[(source_id, 'x')].values
                self.sources.ifs_coordinates[
                    (source_id, 'y', 'value')].iloc[results.index] = results[(source_id, 'y')].values

            # NaN pixels are set to zero since DER_SNR skips such pixels
            snr = der_snr(np.ma.array(spectrum.values, mask=np.isnan(spectrum.values), fill_value=0.).filled())
            logger.info("S/N of source #{0} is {1:.1f}".format(source_id, snr))

            # for PSF stars, determine profile shape, for the origin of the formulae, check the SExtractor manual
            if self.sources.status[source_id] == 3:
                x2 = results[(source_id, 'x2')]
                y2 = results[(source_id, 'y2')]
                xy = results[(source_id, 'xy')]

                a = np.sqrt((x2 + y2) / 2. + np.sqrt(((x2 - y2) / 2.) ** 2 + xy ** 2))
                b = np.sqrt((x2 + y2) / 2. - np.sqrt(((x2 - y2) / 2.) ** 2 + xy ** 2))

                fwhm.append(np.sqrt(8. * np.log(2)) * a)
                e.append(1. - (b / a))
                # according to SExtractor manual, theta should have same size as covariance
                _theta = 0.5 * np.arctan2(2 * xy, (x2 - y2))
                _theta[(_theta < 0) & (xy > 0)] += np.pi/2.
                _theta[(_theta > 0) & (xy < 0)] -= np.pi/2.

                # IMPORTANT: SExtractor measures angle from x-axis in ccw while PampelMuse uses the y-axis as
                # zeropoint. The following lines do the conversion.
                _theta[_theta < 0] += np.pi/2.
                _theta[_theta > 0] -= np.pi/2.

                theta.append(-_theta)

        # calculate (& update) PSF parameters
        logger.info("Estimating PSF properties using measured moments...")
        # check if dispersion information of PSF is correct
        if self.psf_attributes.contains_free_parameters:
            self.psf_attributes.resize(self.sources.wave[results.index.values])

        # calculate wavelength-dependent values of FWHM, ellipticity and theta
        # add warnings filter to suppress messages
        with warnings.catch_warnings():
            warnings.simplefilter('ignore', category=RuntimeWarning)
            mean_fwhm = np.nanmean(fwhm, axis=0)
            mean_e = np.nanmean(e, axis=0)
            mean_theta = np.nanmean(theta, axis=0)

        logger.info("Mean FWHM: {0:.2f}pix.".format(np.nanmean(mean_fwhm)))
        logger.info("Mean ellipticity: {0:.2f}".format(np.nanmean(mean_e)))
        logger.info("Mean position angle: {0:.2f}rad.".format(np.nanmean(mean_theta)))

        # update PSF parameters if the parameters are fitted
        # TODO: should FWHM be updated as well?
        if self.psf_attributes.free['e']:
            self.psf_attributes.data[('e', 'value')] = mean_e
        if self.psf_attributes.free['theta']:
            self.psf_attributes.data[('theta', 'value')] = mean_theta

        # finally bring to same indices as sources instance
        self.psf_attributes.resize(self.sources.wave)

        # determine wavelength-dependent coordinate transformation
        if self.sources.transformation.valid and self.position_fit > 0:
            logger.info("Determining wavelength-dependent coordinate transformation...")
            if self.position_fit == 1:
                fixed = pd.DataFrame({'A': 1., 'B': 0., 'C': 0., 'D': 1.},
                                     index=self.sources.wave[results.index.values])
            else:
                fixed = None
            t = Transformation.from_coordinates(self.sources.catalog, results, fixed=fixed)
            # need to update index-information to match that of sources class
            t.data.index = self.sources.wave[results.index.values]
            t.resize(self.sources.wave)
            #  add to sources instance
            self.sources.transformation = t
