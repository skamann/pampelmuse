"""
__init__.py
===========
Copyright 2013-2019 Sebastian Kamann

This file is part of PampelMuse.

PampelMuse is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

PampelMuse is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PampelMuse. If not, see <http://www.gnu.org/licenses/>.
"""
from .profile import Profile
from .gaussian import Gaussian
from .moffat import Moffat
from .double_gaussian import DoubleGaussian
from .double_moffat import DoubleMoffat
from .moffat_gaussian import MoffatGaussian
from .maoppy import Maoppy

# define what should be imported upon 'from psf import *':
__all__ = ["Gaussian", "Moffat", "DoubleGaussian", "MoffatGaussian", "DoubleMoffat", "Maoppy"]
