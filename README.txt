PampelMuse is a software package with the purpose of facilitating the analysis of integral-field spectroscopic
observations of crowded stellar fields. It provides several subroutines to perform the individual steps of the data
analysis.

The theoretical work that went into the development of this code is described in detail in Kamann, Wisotzki, & Roth
(2013, A&A, 549, A71).

